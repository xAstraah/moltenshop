package me.xastraah.moltenshop;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class MoltenShop extends JavaPlugin implements Listener {

    private String msPrefix = ChatColor.RED + "[MoltenShop] ";

    public static Economy economy = null;

    @SuppressWarnings("deprecation")
    Inventory moltenInventory = Bukkit.createInventory(Bukkit.getServer().getPlayer(getName()), 54, ChatColor.RED + "[MoltenShop]");

    public void onEnable() {
        if (!setupEconomy() ) {
            getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        saveDefaultConfig();
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        economy = rsp.getProvider();
        return economy != null;
    }

    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(msPrefix + "You are not a Player!");
            return true;
        }

        Player p = (Player) sender;

        if(command.getName().equalsIgnoreCase("MoltenShop")) {
            if(args.length == 0) {
                p.openInventory(moltenInventory);
            }
        }

        return false;
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent event) {

        if(event.getInventory().getName().equalsIgnoreCase(moltenInventory.getName())) {
            moltenInventory.clear();
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        Player p = (Player) event.getPlayer();

        if(event.getInventory().getName().equalsIgnoreCase(moltenInventory.getName())) {
            if(moltenInventory.getContents() == null) {
                return;
            }
            
            for(ItemStack is : moltenInventory.getContents()) {
                if(is == null) {
                    return;
                }

                @SuppressWarnings("deprecation")
                EconomyResponse r = economy.depositPlayer(p.getName(), getConfig().getDouble(is.getType().name()) * is.getAmount());

                if(r.transactionSuccess()) {
                    p.sendMessage(msPrefix + "You're new balance is: " + "$" + economy.getBalance(p));
                    return;
                }
            }
        }
    }

}
